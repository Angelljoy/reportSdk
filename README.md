小游戏登录注册，聚合SDK
===============
## 项目目录结构说明:
~~~
CsSdk 				SDK目录
├─utils           	sdk工具目录
│  ├─http.js        请求封装
│  ├─md5.js         md5加密文件
│  ├─utils.js       工具文件
├─api.js           	sdk接口文件（引入使用）
├─config.js         sdk配置文件
├─README.md			说明文件
~~~

## 安装使用:
//TDOO::

```javascript
import CsSdk from './CsSdk/api.js'
//------------------------------------appStartRun调用示例开始------------------------------------
	CsSdk.appStartRun().then(res => {
		console.log('启动调用ok')
		
//------------------------------------登录上报调用示例结束-----------------------------------------
		CsSdk.login().then(loginRes=>{
			//登录接口
			console.log('登录调用ok')
		}).catch(loginErr=>{
			console.log(loginErr)
		})
//------------------------------------登录上报调用示例结束--------------------------------------

//------------------------------------report调用示例开始------------------------------------
		CsSdk.report({
			action_type:'CREATE_ROLE',
			server_id:111,
			server_name:'传奇111服',
			role_id:123456,
			role_name:'张三丰',
			role_level:5,
			role_type:'1',
			role_status:'1',
			vip_level:2,
			gold:20,
			expand:JSON.stringify({
				bbb:'角色上报扩展数据不超过200字符'
			}) || '{}' //扩展参数，json字符串形式传入，如果没有就传个字符串'{}'
		}).then(reportRes=>{
			console.log('上报调用ok')
		}).catch(reportErr=>{
			console.log(reportErr)
		})
//------------------------------------report调用示例结束------------------------------------	
	
		CsSdk.payOrder({
			role_id:'435',//角色ID
			server_id:'34345',//区服ID
			prop_id:'',//道具ID,qq,美团小游戏必传，其它渠道不传递
			sku_id:'123456',//游戏对应的商品ID，最小粒度，可空
			buy_num:1,//购买道具数量,不传递默认为1，注意不是游戏币数量，例如：一个道具6元，获得60个币，购买三个道具就是3，pay_amount就是600分 x数量3=1800分
			cp_order_id:new Date().getTime()+ Math.floor(Math.random() * 10000),//订单号，系统下唯一，最长允许50位，每次发起支付都要是新的单号，如果拉起支付后未支付，关闭支付窗口，再次发起需要更换单号
			pay_amount:100,//支付总金额，单位：分
			title:'钻石购买',
			remark:'备注信息不超过30个字符',
			pay_callback_url:'',//此参数存在时，回调以此参数为准，不传递默认使用后台配置的默认回调地址
			expand:JSON.stringify({//扩展参数，json字符串形式传入，如果没有就传个字符串'{}'
				aaa:'订单扩展数据不超过200字符'
			}) || '{}',
			env:0,
		}).then(paySucc=>{
			console.log('支付调用ok',paySucc)
			
		}).catch(payErr=>{
			console.log('支付调用失败',payErr)
		})
		
		
//------------------------------------内容检测调用示例开始------------------------------------
		CsSdk.checkTextContent({
			'content':'你好'
		}).then(checkRes=>{
			//通过
			console.log('内容检测调用ok,检测结果：通过')
		}).catch(checkErr=>{
			//不通过
			console.log('内容检测调用ok,检测结果：不通过')
		})
//------------------------------------内容检测调用示例结束------------------------------------


//------------------------------------页面访问调用示例开始------------------------------------		
	}).catch(res => {
		console.log('启动失败', res)
	})
//------------------------------------appStartRun调用示例结束------------------------------------			
```
