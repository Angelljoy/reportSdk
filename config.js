// config.js
export const deBug = false;//是否开启调试、值：true|false，true会打印请求log
export const baseUrl = 'https://sdkapi.haotianhuyu.com';
// export const baseUrl = 'http://localhost:8000';

// 微信
// export const appId = '202312065958'; //传盛分配的appId，12位
// export const secret = 'CAE6DC5422611B567B62E03DC980A0A6'; //传盛分配的密钥，32位，大写
// export const tagName = 'wx'; //sdk渠道标识，必须有引号！！！微信:'wx'、QQ:'qq'、支付宝:'my'、美团:'mt'，抖音:'tt'
// export const tagObj = wx; //sdk渠道对象，没有引号！！！微信:wx、QQ:qq、支付宝:my、美团:mt，抖音:tt
